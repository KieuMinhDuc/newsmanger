package com.homework.newsmanager.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.homework.newsmanager.database.dao.NewsDao;
import com.homework.newsmanager.database.dao.NewsTypeDao;
import com.homework.newsmanager.database.entity.News;
import com.homework.newsmanager.database.entity.NewsType;

@Database(entities = {News.class, NewsType.class}, version = 1, exportSchema = false)
public abstract class LocalDatabase extends RoomDatabase {
    public static volatile LocalDatabase INSTANCE;

    public abstract NewsDao newsDao();
    public abstract NewsTypeDao newsTypeDao();

    public static LocalDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (LocalDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            LocalDatabase.class, "news_database")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}