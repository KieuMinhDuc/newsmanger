package com.homework.newsmanager.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.homework.newsmanager.utils.DateConverter;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "news")
public class News implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id_news")
    private Long idNews;

    @NonNull
    @ColumnInfo(name = "title")
    String title;

    @NonNull
    @ColumnInfo(name = "descriptionNews")
    String descriptionNews;

    @ColumnInfo(name = "created_date")
    @TypeConverters({DateConverter.class})
    public Date createDate;

    @Embedded
    NewsType newsType;

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @NonNull
    public Long getIdNews() {
        return idNews;
    }

    public void setIdNews(@NonNull Long idNews) {
        this.idNews = idNews;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    @NonNull
    public String getDescriptionNews() {
        return descriptionNews;
    }

    public void setDescriptionNews(@NonNull String descriptionNews) {
        this.descriptionNews = descriptionNews;
    }

    public NewsType getNewsType() {
        return newsType;
    }

    public void setNewsType(NewsType newsType) {
        this.newsType = newsType;
    }
}
