package com.homework.newsmanager.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.homework.newsmanager.database.entity.News;

import java.util.List;

@Dao
public interface NewsDao {
    @Query("SELECT * from news")
    List<News> getAllNews();

    @Insert
    void addNews(News News);

    @Query("DELETE FROM News")
    void deleteAllNews();

    @Update
    int updateNews(News News);

    @Delete
    void deleteNews(News News);
}
