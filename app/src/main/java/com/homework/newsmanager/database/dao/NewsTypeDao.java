package com.homework.newsmanager.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.homework.newsmanager.database.entity.NewsType;

import java.util.List;

@Dao
public interface NewsTypeDao {
    @Query("SELECT * from newstype")
    List<NewsType> getAllNewsType();

    @Insert
    void addNewsType(NewsType newsType);

    @Query("DELETE FROM newstype")
    void deleteAllNewsType();

    @Update
    int updateNewType(NewsType newsType);

    @Delete
    void deleteNewsType(NewsType newsType);

//    @Query("DELETE FROM newstype WHERE url = :urlFavorite")
//    void deleteNewsType(String urlFavorite);
//
//    @Query("SELECT * from newstype WHERE url = :urlFavorite")
//    Favorite isAddFavorite(String urlFavorite);
}
