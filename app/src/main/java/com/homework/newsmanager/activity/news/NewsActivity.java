package com.homework.newsmanager.activity.news;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.homework.newsmanager.R;
import com.homework.newsmanager.constants.Constants;
import com.homework.newsmanager.adapter.NewsAdapter;
import com.homework.newsmanager.callback.OnItemClickListener;
import com.homework.newsmanager.database.LocalDatabase;
import com.homework.newsmanager.database.entity.News;
import com.homework.newsmanager.database.entity.NewsType;
import com.homework.newsmanager.utils.DateConverter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NewsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private List<News> newsList;
    private NewsAdapter newsAdapter;

    private NewsType fitlerNewsType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        getSupportActionBar().setTitle("Danh sách tin tức");
        findViewById(R.id.ic_add_news).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsActivity.this, AddNewsActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    private void initData() {
        newsList = LocalDatabase.INSTANCE.newsDao().getAllNews();
        recyclerView = findViewById(R.id.rv_list_news);
        recyclerView.setHasFixedSize(true);

        newsAdapter = new NewsAdapter(newsList, new OnItemClickListener() {
            @Override
            public void onItemNewsClick(News news) {
                Intent intent = new Intent(NewsActivity.this, EditNewsActivity.class);
                intent.putExtra(Constants.KEY_NEWS, news);
                startActivity(intent);

            }
        });
        recyclerView.setAdapter(newsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(NewsActivity.this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_fitler) {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Lọc");

            LayoutInflater li = LayoutInflater.from(this);
            final View myView = li.inflate(R.layout.activity_fitler, null);
            // type
            Spinner snChooseNewsType = myView.findViewById(R.id.spinner_filter_type);
            List<NewsType> newsTypeList = LocalDatabase.INSTANCE.newsTypeDao().getAllNewsType();
            List<String> listItemFromSpinner = new ArrayList<>();
            for (NewsType newsType : newsTypeList) {
                listItemFromSpinner.add(newsType.getName());
            }
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, listItemFromSpinner);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            snChooseNewsType.setAdapter(dataAdapter);
            snChooseNewsType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    fitlerNewsType = newsTypeList.get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            //date
            EditText edFromDate = myView.findViewById(R.id.edt_fitler_from_date);
            edFromDate.setInputType(InputType.TYPE_NULL);
            edFromDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(NewsActivity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    edFromDate.setText(year + "-" + + (monthOfYear + 1) + "-" + dayOfMonth);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }

            });

            EditText edToDate = myView.findViewById(R.id.edt_fitler_to_date);
            edToDate.setInputType(InputType.TYPE_NULL);
            edToDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Get Current Date
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);


                    DatePickerDialog datePickerDialog = new DatePickerDialog(NewsActivity.this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    edToDate.setText(year + "-" + + (monthOfYear + 1) + "-" + dayOfMonth);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }

            });




            alert.setView(myView);
            alert.setCancelable(false);
            alert.setNegativeButton("Cancel", null);

            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    List<News> listTemp = new ArrayList<>();
                    Date dateFrom = DateConverter.fromTimestamp(edFromDate.getText().toString());
                    Date dateTo = DateConverter.fromTimestamp(edToDate.getText().toString());
                    for (News news: newsList) {
                        if (news.getNewsType().getName().equals(fitlerNewsType.getName())
                                && news.getCreateDate().before(dateTo) && news.getCreateDate().after(dateFrom)) {
                            listTemp.add(news);
                        }
                    }
                    newsAdapter = new NewsAdapter(listTemp, new OnItemClickListener() {
                        @Override
                        public void onItemNewsClick(News news) {
                            Intent intent = new Intent(NewsActivity.this, EditNewsActivity.class);
                            intent.putExtra(Constants.KEY_NEWS, news);
                            startActivity(intent);

                        }
                    });
                    recyclerView.setAdapter(newsAdapter);
                }
            });
            AlertDialog dialog = alert.create();
            dialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
