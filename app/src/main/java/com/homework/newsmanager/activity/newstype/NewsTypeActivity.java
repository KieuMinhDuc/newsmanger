package com.homework.newsmanager.activity.newstype;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.homework.newsmanager.R;
import com.homework.newsmanager.constants.Constants;
import com.homework.newsmanager.adapter.NewsTypeAdapter;
import com.homework.newsmanager.callback.OnItemClickListener;
import com.homework.newsmanager.database.LocalDatabase;
import com.homework.newsmanager.database.entity.NewsType;

import java.util.List;

public class NewsTypeActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<NewsType> newsTypeList;
    private NewsTypeAdapter newsTypeAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_type);
        Toolbar toolbar = findViewById(R.id.toolbar_news_type);
        toolbar.setTitle("Loại Tin tức");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        findViewById(R.id.ic_add_news_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsTypeActivity.this, AddNewsTypeActivity.class));
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
    }

    private void initData() {
        newsTypeList = LocalDatabase.INSTANCE.newsTypeDao().getAllNewsType();
        recyclerView = findViewById(R.id.rv_list_news_type);
        recyclerView.setHasFixedSize(true);

        newsTypeAdapter = new NewsTypeAdapter(newsTypeList, new OnItemClickListener() {
            @Override
            public void onItemNewsTypeClick(NewsType newsType) {
                Intent intent = new Intent(NewsTypeActivity.this, EditNewsTypeActivity.class);
                intent.putExtra(Constants.KEY_NEWS_TYPE, newsType);
                startActivity(intent);

            }
        });
        recyclerView.setAdapter(newsTypeAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(NewsTypeActivity.this));
    }
}
