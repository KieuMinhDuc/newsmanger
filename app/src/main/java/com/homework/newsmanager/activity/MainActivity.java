package com.homework.newsmanager.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.homework.newsmanager.R;
import com.homework.newsmanager.activity.news.NewsActivity;
import com.homework.newsmanager.activity.newstype.NewsTypeActivity;
import com.homework.newsmanager.database.LocalDatabase;
import com.homework.newsmanager.database.entity.NewsType;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // move to screen news
        findViewById(R.id.btn_news).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NewsActivity.class));
            }
        });

        // move to screen news type
        findViewById(R.id.btn_news_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NewsTypeActivity.class));
            }
        });
    }
}
