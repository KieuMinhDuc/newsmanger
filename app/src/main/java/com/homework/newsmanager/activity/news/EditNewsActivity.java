package com.homework.newsmanager.activity.news;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.homework.newsmanager.R;
import com.homework.newsmanager.constants.Constants;
import com.homework.newsmanager.database.LocalDatabase;
import com.homework.newsmanager.database.entity.News;
import com.homework.newsmanager.database.entity.NewsType;
import com.homework.newsmanager.utils.DateConverter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EditNewsActivity extends AppCompatActivity {

    private News news;
    private NewsType newsType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);
        Toolbar toolbar = findViewById(R.id.toolbar_add_news);
        toolbar.setTitle("Sửa Tin tức");

        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        Bundle bundle = getIntent().getExtras();
        news = (News) bundle.getSerializable(Constants.KEY_NEWS);

        EditText edtName = findViewById(R.id.edt_name);
        edtName.setText(news.getTitle());
        EditText edtDescription = findViewById(R.id.edt_description);
        edtDescription.setText(news.getDescriptionNews());

        //date
        EditText edDate = findViewById(R.id.edt_date);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        edDate.setText(df.format(news.getCreateDate()));
        edDate.setInputType(InputType.TYPE_NULL);
        edDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(EditNewsActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                edDate.setText(year + "-" + + (monthOfYear + 1) + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }

        });


        // type
        Spinner snChooseNewsType = findViewById(R.id.spinner_news_type);
        List<NewsType> newsTypeList = LocalDatabase.INSTANCE.newsTypeDao().getAllNewsType();
        List<String> listItemFromSpinner = new ArrayList<>();
        for (NewsType newsType : newsTypeList) {
            if (newsType.getName().equals(news.getNewsType().getName())) {

                snChooseNewsType.post(new Runnable() {
                    @Override
                    public void run() {
                        snChooseNewsType.setSelection(newsTypeList.indexOf(newsType));

                    }
                });
            }
            listItemFromSpinner.add(newsType.getName());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, listItemFromSpinner);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        snChooseNewsType.setAdapter(dataAdapter);


        snChooseNewsType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                newsType = newsTypeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        findViewById(R.id.ic_done_news).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edDate.getText().toString().isEmpty() || newsTypeList.isEmpty() || edtName.getText().toString().isEmpty() || edtDescription.getText().toString().isEmpty() ) {
                    showAlert("Lỗi", "Không được để trống các trường");
                } else {
                    news.setTitle(edtName.getText().toString());
                    news.setDescriptionNews(edtDescription.getText().toString());
                    news.setNewsType(newsType);
                    news.setCreateDate(DateConverter.fromTimestamp(edDate.getText().toString()));
                    LocalDatabase.INSTANCE.newsDao().updateNews(news);
                    finish();
                }
            }
        });

    }

    public void showAlert(String titleResId, String messageResId) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        new AlertDialog.Builder(EditNewsActivity.this).setTitle(titleResId)
                                .setMessage(messageResId)
                                .setNegativeButton(android.R.string.ok, null).create()
                                .show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
