package com.homework.newsmanager.activity.newstype;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.homework.newsmanager.R;
import com.homework.newsmanager.constants.Constants;
import com.homework.newsmanager.database.LocalDatabase;
import com.homework.newsmanager.database.entity.NewsType;

public class EditNewsTypeActivity extends AppCompatActivity {

    private NewsType newsType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news_type);
        Toolbar toolbar = findViewById(R.id.toolbar_add_news_type);
        toolbar.setTitle("Sửa Loại Tin tức");

        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));

        Bundle bundle = getIntent().getExtras();
        newsType = (NewsType) bundle.getSerializable(Constants.KEY_NEWS_TYPE);

        EditText edtName = findViewById(R.id.edt_name);
        edtName.setText(newsType.getName());
        EditText edtDescription = findViewById(R.id.edt_description);
        edtDescription.setText(newsType.getDescription());
        findViewById(R.id.ic_done_news_type).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtName.getText().toString().isEmpty() || edtDescription.getText().toString().isEmpty() ) {
                    showAlert("Lỗi", "Không được để trống các trường");
                } else {
                    newsType.setName(edtName.getText().toString());
                    newsType.setDescription(edtDescription.getText().toString());
                    LocalDatabase.INSTANCE.newsTypeDao().updateNewType(newsType);
                    finish();
                }
            }
        });

    }

    public void showAlert(String titleResId, String messageResId) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        new AlertDialog.Builder(EditNewsTypeActivity.this).setTitle(titleResId)
                                .setMessage(messageResId)
                                .setNegativeButton(android.R.string.ok, null).create()
                                .show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
