package com.homework.newsmanager.constants;

public class Constants {
    public static final String KEY_IS_EDIT = "KEY_IS_EDIT";
    public static final String EVENT_EDIT = "EVENT_EDIT";
    public static final String EVENT_DELETE = "EVENT_DELETE";
    public static final String KEY_NEWS_TYPE = "KEY_NEWS_TYPE";
    public static final String KEY_NEWS = "KEY_NEWS";
}
