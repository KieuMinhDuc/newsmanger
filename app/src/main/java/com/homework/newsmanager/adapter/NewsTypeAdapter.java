package com.homework.newsmanager.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.homework.newsmanager.R;
import com.homework.newsmanager.callback.OnItemClickListener;
import com.homework.newsmanager.database.LocalDatabase;
import com.homework.newsmanager.database.entity.NewsType;

import org.w3c.dom.Text;

import java.util.List;

public class NewsTypeAdapter extends RecyclerView.Adapter {

    private List<NewsType> newsTypesList;
    private OnItemClickListener onItemClickListener;
    public NewsTypeAdapter(List<NewsType> newsTypeList, OnItemClickListener onItemClickListener) {
        this.newsTypesList = newsTypeList;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new_stype, parent, false);
        return new NewsTypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NewsTypeViewHolder newsTypeViewHolder = (NewsTypeViewHolder) holder;
        newsTypeViewHolder.title.setText(newsTypesList.get(position).getName());
        newsTypeViewHolder.description.setText(newsTypesList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return newsTypesList.size();
    }

    class NewsTypeViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView description;
        ImageView ivEdit;
        ImageView ivDelete;
        public NewsTypeViewHolder(@NonNull View view) {
            super(view);
            title = view.findViewById(R.id.tv_news_type_name);
            description = view.findViewById(R.id.tv_news_type_description);
            ivEdit = view.findViewById(R.id.iv_edit);
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemNewsTypeClick(newsTypesList.get(getAdapterPosition()));
                }
            });
            ivDelete = view.findViewById(R.id.iv_delete);
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocalDatabase.INSTANCE.newsTypeDao().deleteNewsType(newsTypesList.get(getAdapterPosition()));
                    newsTypesList.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    notifyItemRangeChanged(getAdapterPosition(), newsTypesList.size());
                }
            });
        }
    }
}
