package com.homework.newsmanager.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.homework.newsmanager.R;
import com.homework.newsmanager.callback.OnItemClickListener;
import com.homework.newsmanager.database.LocalDatabase;
import com.homework.newsmanager.database.entity.News;
import com.homework.newsmanager.database.entity.NewsType;

import java.text.SimpleDateFormat;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter {

    private List<News> newsList;
    private OnItemClickListener onItemClickListener;
    public NewsAdapter(List<News> newsList, OnItemClickListener onItemClickListener) {
        this.newsList = newsList;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new, parent, false);
        return new NewsTypeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NewsTypeViewHolder newsTypeViewHolder = (NewsTypeViewHolder) holder;
        newsTypeViewHolder.title.setText(newsList.get(position).getTitle());
        newsTypeViewHolder.description.setText(newsList.get(position).getDescriptionNews());
        newsTypeViewHolder.type.setText(newsList.get(position).getNewsType().getName());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        newsTypeViewHolder.date.setText(df.format(newsList.get(position).getCreateDate()));
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    class NewsTypeViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView description;
        TextView date;
        TextView type;
        ImageView ivEdit;
        ImageView ivDelete;
        public NewsTypeViewHolder(@NonNull View view) {
            super(view);
            title = view.findViewById(R.id.tv_news_name);
            description = view.findViewById(R.id.tv_news_description);
            date = view.findViewById(R.id.tv_news_date);
            type = view.findViewById(R.id.tv_news_type);
            ivEdit = view.findViewById(R.id.iv_edit);
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemNewsClick(newsList.get(getAdapterPosition()));
                }
            });
            ivDelete = view.findViewById(R.id.iv_delete);
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LocalDatabase.INSTANCE.newsDao().deleteNews(newsList.get(getAdapterPosition()));
                    newsList.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    notifyItemRangeChanged(getAdapterPosition(), newsList.size());
                }
            });
        }
    }
}
