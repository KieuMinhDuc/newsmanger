package com.homework.newsmanager.callback;

import com.homework.newsmanager.database.entity.News;
import com.homework.newsmanager.database.entity.NewsType;

public interface OnItemClickListener {
    default void onItemNewsClick(News news) {
    }

    default void onItemNewsTypeClick(NewsType newsType) {
    }
}
