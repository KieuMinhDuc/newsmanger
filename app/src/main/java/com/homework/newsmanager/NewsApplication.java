package com.homework.newsmanager;

import android.app.Application;

import com.homework.newsmanager.database.LocalDatabase;

public class NewsApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // init local database
        LocalDatabase.getDatabase(this);

    }
}
